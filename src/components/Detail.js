import React from 'react';
import { Card, ListGroup, ListGroupItem } from 'react-bootstrap'


const Detail = ({ detail }) => {

    const myStyle = {
        width: "50%",
        marginLeft: "auto",
        marginRight: "auto",

    };

    return (
        <div>
            <Card style={{ width: '100%' }}>
                <Card.Img variant="top" style={myStyle} src="/images/profile.png" />
                <Card.Body>
                    <Card.Title>{detail.name}</Card.Title>

                    <ListGroup className="list-group-flush">
                        <ListGroupItem>Email: {detail.email}</ListGroupItem>
                        <ListGroupItem>Phone No: {detail.phone}</ListGroupItem>
                        <ListGroupItem>Company Name: {detail.companyName}</ListGroupItem>
                        <ListGroupItem>Address: {detail.address}</ListGroupItem>
                    </ListGroup>

                </Card.Body>

            </Card>
        </div>
    );
};

export default Detail;