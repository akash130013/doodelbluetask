export default function validate(values) {

    let errors = {};
    if (!values.email) {
        errors.email = 'Email address is required';
    } else if (!/\S+@\S+\.\S+/.test(values.email)) {
        errors.email = 'Email address is invalid';
    }

    if (!values.name) {
        errors.name = 'Name is required'
    }

    if (!values.address) {
        errors.address = 'Address is required'
    }

    if (!values.companyName) {
        errors.companyName = 'Company name is required'
    }

    if (!values.phone) {
        errors.phone = 'Phone number is required'
    }

    return errors;
};