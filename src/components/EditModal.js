import React,{useEffect,useState} from 'react'
import { useForm } from "react-hook-form";
import { Modal, Button, Form } from 'react-bootstrap'

export default function EditModal(props) {

//   const {name,email,companyName,address,phone}=props.handleEdit

 let editValues={
 };

//  console.log("11",Object.keys(props.handleEdit).length,props.handleEdit);


   
  if(Object.keys(props.handleEdit).length){

    console.log("12334",props.handleEdit.name);
    // editValues.name=props.handleEdit.name

      editValues={
          name:props.handleEdit.name,
          companyName:props.handleEdit.companyName,
          email:props.handleEdit.email,
          address:props.handleEdit.address,
          phone:props.handleEdit.phone,
      };

   
  }




  console.log("valida",editValues);
  
    const { handleSubmit, register, errors } = useForm({
        defaultValues:editValues
    });
    const onSubmit = values => props.onFormSubmit(values)

    const handleClose = () => {
        props.onChangeModel(props.isOpen);
    }


    return (
        <>

            <Modal show={props.isOpen} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Add Contact</Modal.Title>
                </Modal.Header>
                <Form onSubmit={handleSubmit(onSubmit)}>
                    <Modal.Body>

                        <Form.Group controlId="formBasicName">
                            <Form.Label>Full Name</Form.Label>
                            <Form.Control type="text" placeholder="Full Name" name="name"
                                ref={register({
                                    required: "Required",
                                })}
                            />
                            {errors.name && errors.name.message}

                        </Form.Group>

                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" name="email"
                                ref={register({
                                    required: "Required",
                                    pattern: {
                                        value: /^[A-Z0-9._%+-]+@[A-Z0-9.-]+\.[A-Z]{2,}$/i,
                                        message: "invalid email address"
                                    }
                                })}
                            />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                  </Form.Text>
                            {errors.email && errors.email.message}
                        </Form.Group>

                        <Form.Group controlId="formBasicPhone">
                            <Form.Label>Phone No</Form.Label>
                            <Form.Control type="text" placeholder="Phone Number" name="phone" 
                              ref={register({
                                required: "Required",
                            })}
                            />
                            {errors.phone && errors.phone.message}
                        </Form.Group>

                        <Form.Group controlId="formBasicCompany">
                            <Form.Label>Company Name</Form.Label>
                            <Form.Control type="text" placeholder="Company Name" name="companyName" 
                             ref={register({
                                required: "Required",
                            })}
                            />
                           {errors.companyName && errors.companyName.message}
                        </Form.Group>

                        <Form.Group controlId="formBasicAddress">
                            <Form.Label>Address</Form.Label>
                            <Form.Control type="text" placeholder="Address" name="address" 
                             ref={register({
                                required: "Required",
                            })}
                            />
                            {errors.address && errors.address.message}
                        </Form.Group>

                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
               </Button>
                        <Button type="submit" variant="primary">
                            Save Changes
               </Button>

                    </Modal.Footer>
                </Form>
            </Modal>

        </>
    )
}
