import React from 'react';
import { Modal, Button, Form } from 'react-bootstrap'
import validate from './FormValidation'
import useForm from './useForm'


const CreateModel = (props) => {

    const {
        values,
        errors,
        handleChange,
        handleSubmit,
    } = useForm(submitForm, validate);


    function submitForm() {
        console.log('No errors, submit callback called!');
        props.onFormSubmit(values);
        handleClose();
    }



    const handleClose = () => {
        props.onChangeModel(props.isOpen);
    }



    return (

        <>

            <Modal show={props.isOpen} onHide={handleClose}>
                <Modal.Header closeButton>
                    <Modal.Title>Add Contact</Modal.Title>
                </Modal.Header>
                <Form onSubmit={handleSubmit}>
                    <Modal.Body>

                        <Form.Group controlId="formBasicName">
                            <Form.Label>Full Name</Form.Label>
                            <Form.Control type="text" placeholder="Full Name" name="name" onChange={handleChange} value={values.name || ''} />
                            {errors.name && (
                                <p className="help is-danger">{errors.name}</p>
                            )}
                        </Form.Group>

                        <Form.Group controlId="formBasicEmail">
                            <Form.Label>Email address</Form.Label>
                            <Form.Control type="email" placeholder="Enter email" name="email" onChange={handleChange} value={values.email || ''} />
                            <Form.Text className="text-muted">
                                We'll never share your email with anyone else.
                      </Form.Text>
                            {errors.email && (
                                <p className="help is-danger">{errors.email}</p>
                            )}
                        </Form.Group>

                        <Form.Group controlId="formBasicPhone">
                            <Form.Label>Phone No</Form.Label>
                            <Form.Control type="text" placeholder="Phone Number" name="phone" onChange={handleChange} value={values.phone || ''}  />
                            {errors.phone && (
                                <p className="help is-danger">{errors.phone}</p>
                            )}
                        </Form.Group>

                        <Form.Group controlId="formBasicCompany">
                            <Form.Label>Company Name</Form.Label>
                            <Form.Control type="text" placeholder="Company Name" name="companyName" onChange={handleChange} value={values.companyName || ''} />
                            {errors.companyName && (
                                <p className="help is-danger">{errors.companyName}</p>
                            )}
                        </Form.Group>

                        <Form.Group controlId="formBasicAddress">
                            <Form.Label>Address</Form.Label>
                            <Form.Control type="text" placeholder="Address" name="address" onChange={handleChange} value={values.address || ''} />
                            {errors.address && (
                                <p className="help is-danger">{errors.address}</p>
                            )}
                        </Form.Group>



                    </Modal.Body>
                    <Modal.Footer>
                        <Button variant="secondary" onClick={handleClose}>
                            Close
                   </Button>
                        <Button type="submit" variant="primary">
                            Save Changes
                   </Button>

                    </Modal.Footer>
                </Form>
            </Modal>

        </>
    )
};

export default CreateModel;