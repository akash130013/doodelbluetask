import React from 'react';
import { Container, Col, Row, Form, Button, Table } from 'react-bootstrap'
// import Modal from './components/CreateModel'
import UserDetail from './components/Detail'
import Modal from './components/EditModal'




class App extends React.Component {

    state = {
        isOpen: false,
        formData: [],
        editData:{},
        query:'',

    }

    handleOpen = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    handleModelChange = (val) => {

        this.setState({
            isOpen: !val
        })
    }

    submitForm = (data) => {

        this.setState({
            formData: this.state.formData.concat(data),
            isOpen: false
        })

    }

    renderList = () => {

        if (!this.state.formData) {
            return <div>Loading...</div>
        }

        return this.state.formData.filter((item) =>item.name.includes(this.state.query)).map((data, index) => {
            return (
                <tr key={index}>
                    <td>{Number(index) + 1}</td>
                    <td>{data.name}</td>
                    <td>{data.companyName}</td>
                    <td><ul>
                        <li> <i className="fa fa-pencil-square" aria-hidden="true" onClick={() => this.handleEdit(index)}></i></li>
                        <li><i className="fa fa-eye" aria-hidden="true" onClick={() => this.showDetail(index)} ></i></li>
                    </ul>
                    </td>
                </tr>
            )
        })

    }

    showDetail = (index) => {

        this.setState({
            detailData: this.state.formData[index],
            
        })

    }

    handleEdit = (index)=>{
        this.setState({
            editData: this.state.formData[index],
            isOpen: true,
        })

    }


    handleSearch=(e)=>{
        // console.log("11",e.target.value);
        this.setState({
            query: e.target.value,
        })

    }


    render() {
       
        return (
            <div>
                <Container>
                    <Row>
                        <Col sm={8}>
                            <h3>Create Record</h3>

                            <Row>
                                <Col sm={8}>
                                    <Form.Group>
                                        <Form.Control type="search" placeholder="Type something" onChange={this.handleSearch}  />
                                    </Form.Group>
                                </Col>
                                <Col sm={4}>
                                    <Button variant="success" onClick={this.handleOpen}>+ Add Contact</Button>
                                    <Modal isOpen={this.state.isOpen} handleEdit={this.state.editData} onChangeModel={this.handleModelChange} onFormSubmit={this.submitForm} />
                                </Col>
                            </Row>
                            <Table striped bordered hover size="sm">
                                <thead>
                                    <tr>
                                        <th>Sr.No</th>
                                        <th>Name</th>
                                        <th>Company</th>
                                        <th>Action</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    {this.renderList()}
                                </tbody>
                            </Table>
                        </Col>
                        <Col sm={4}>
                            <h3>Detail</h3>
                            {this.state.detailData && Object.keys(this.state.detailData).length > 0 && (<UserDetail detail={this.state.detailData} />)}
                        </Col>
                    </Row>
                </Container>
            </div >
        )
    }

}


export default App;